# DWBootstrap #

Troll XNAT contents and report on usage that effects DICOMweb in XNAT

### Build ###

mvn clean install

### Run ###

See the sh script dwreport.sh

### Usage ###

dwreport.sh --archiveRootPath <archiveRootPath> [--projectLabel <projectLabel> [--subjectLabel <subjectLabel> [--sessionLabel <sessionLabel>]]]