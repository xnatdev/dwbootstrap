package org.nrg.dicomweb.bootstrap.reporter;

import org.nrg.xnat.datamodel.ImageSessionData;

public class ImageSessionDataDBSignature {
    private String id;
    private String sessionLabel;
    private String dcmPatientName;
    private String dcmPatientID;
    private String dcmAccessionNumber;
    private String modality;
    private String studyID;

    public ImageSessionDataDBSignature(ImageSessionData sessionData) {
        this.id = sessionData.getID();
        this.sessionLabel = sessionData.getLabel();
        this.dcmAccessionNumber = sessionData.getDcmAccessionNumber();
        this.dcmPatientID = sessionData.getDcmPatientId();
        this.dcmPatientName = sessionData.getDcmPatientName();
        this.modality = sessionData.getModality();
        this.studyID = sessionData.getStudyId();
    }
}
