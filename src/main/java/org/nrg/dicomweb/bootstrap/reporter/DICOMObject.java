package org.nrg.dicomweb.bootstrap.reporter;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.io.DicomInputStream;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

public class DICOMObject {
    private Attributes dataset;

    public DICOMObject( Path path) throws IOException {
        this( path.toFile());
    }
    public DICOMObject( File file) throws IOException {
        DicomInputStream dis = new DicomInputStream( file);
        dataset = dis.readDatasetUntilPixelData();
    }

    public static Optional<DICOMObject> pickOne( File srcFile) throws IOException {
        if (srcFile.isFile()) {
            return (srcFile.getName().endsWith("dcm"))? Optional.of( new DICOMObject( srcFile)): Optional.empty();
        }
        else {
            for( File f: srcFile.listFiles()) {
                Optional<DICOMObject> dicomObject = pickOne( f);
                if( dicomObject.isPresent()) {
                    return dicomObject;
                }
            }
        }
        return Optional.empty();
    }

    public String getAttribute( int tag) {
        return dataset.getString( tag);
    }

    public String getPatientName() {
        return getAttribute( 0x00100010);
    }
    public String getPatientID() {
        return getAttribute( 0x00100020);
    }
    public String getAccessionNumber() {
        return getAttribute( 0x00080050);
    }
    public String getModality() {
        return getAttribute( 0x00080060);
    }
    public String getStudyID() {
        return getAttribute( 0x00200010);
    }
}
