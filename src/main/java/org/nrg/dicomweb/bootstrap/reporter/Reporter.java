package org.nrg.dicomweb.bootstrap.reporter;

import java.io.PrintStream;

public interface Reporter {
    default void report( PrintStream ps) { ps.println("Not overriden.");}
    default void report( String projectLabel, String subjectLabel, PrintStream ps) { report( ps);}
}
