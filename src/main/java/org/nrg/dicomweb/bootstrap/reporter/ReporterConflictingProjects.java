package org.nrg.dicomweb.bootstrap.reporter;

import org.nrg.xnatClient.XNATClient;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;

public class ReporterConflictingProjects implements Reporter {
    private XNATClient xnatClient;

    public ReporterConflictingProjects(XNATClient xnatClient) {
        this.xnatClient = xnatClient;
    }

    private void reportTitle( PrintStream ps) {
        ps.println("Projects With Duplicate Study Instance UIDs");
    }

    private static String recordFormat = "%s,%s";

    public void reportHeader( PrintStream ps) {
        ps.println( String.format(recordFormat, "Project","Conflicting Project"));
    }

    public void reportRecord( String projectLabel, String conflictingProjectLabel, PrintStream ps) {
        ps.println( String.format( recordFormat, projectLabel, conflictingProjectLabel));
    }

    public void report( PrintStream ps) {
        reportTitle( ps);
        ps.println();

        Map<String, Integer> studyUIDs = xnatClient.getProjectCountPerStudyInstanceUID();

        List<String> dupStudyUIDs = studyUIDs.keySet().stream()
                .filter( suid -> studyUIDs.get( suid) > 2)
                .collect(Collectors.toList());

        reportHeader( ps);
        Set<String> set = new HashSet<>();
        Set<Set<String>> setset = new HashSet<>();

        List<String> projectLabels = xnatClient.getProjectLabels();
        set.addAll( projectLabels);
        for( String studyUID: dupStudyUIDs) {
            setset.add( new HashSet<>( xnatClient.getProjectLabelsByStudyInstanceUID(studyUID)));
        }
        Map<String, Set<String>> partition = partition( projectLabels, setset);
//        for( String pLabel: partition.keySet()) {
//            ps.println(pLabel + ",");
//            partition.get( pLabel).stream().forEachOrdered( p-> reportRecord("", p, ps));
//        }
        partition.keySet().stream()
                .filter( p -> ! p.matches("\\d{6}"))
                .sorted()
                .forEach( p -> {
                    reportRecord( p, "", ps);
                    partition.get(p).stream().sorted().forEach( pc -> reportRecord("", pc, ps));
                });

    }

    public Set<String> subset( String element, Set<Set<String>> setset) {
        Set<String> subset = new HashSet<>();
        setset.forEach( set -> {
            if( set.contains( element)) {
                subset.addAll( set);
                subset.remove( element);
            }
        });
        return subset;
    }

    public Map<String, Set<String>> partition( List<String> elements, Set<Set<String>> setset) {
        return elements.stream()
                .collect(Collectors.toMap( e -> e, e -> subset( e, setset)));
    }


    @Override
    public void report(String projectLabel, String subjectLabel, PrintStream ps) {
        report( ps);
    }
}
