package org.nrg.dicomweb.bootstrap.reporter;

import org.nrg.xnat.datamodel.ImageScanData;
import org.nrg.xnat.datamodel.ImageSessionData;
import org.nrg.xnat.datamodel.ResourceCatalog;
import org.nrg.xnat.datamodel.SubjectData;
import org.nrg.xnatClient.XNATClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ReporterDBvsDICOM implements Reporter {
    private XNATClient xnatClient;

    public ReporterDBvsDICOM( XNATClient xnatClient) {
        this.xnatClient = xnatClient;
    }

    public void report( String projectLabel, String subjectLabel, Path archiveRootPath, PrintStream ps) throws IOException {
        SubjectData subjectData = xnatClient.getSubject(projectLabel, subjectLabel);
        List<ImageSessionData> sessionDataList = subjectData.getExperiments().getExperiment().stream()
                .filter(ImageSessionData.class::isInstance)
                .map(ImageSessionData.class::cast)
                .collect(Collectors.toList());

        reportHeader( ps);

        for (ImageSessionData sessionData : sessionDataList) {
            List<ImageScanData> imageScanDataList = sessionData.getScans().getScan();
            for (ImageScanData scanData : imageScanDataList) {

                Optional<ResourceCatalog> catalog = scanData.getFile().stream()
                        .filter(ResourceCatalog.class::isInstance)
                        .map( ResourceCatalog.class::cast)
                        .findAny();
                if( catalog.isPresent()) {
                    Path catalogFilePath = Paths.get( catalog.get().getURI());
//                            Path dicomDirPath = archiveRootPath.resolve(String.format("%s/arc001/%s/SCANS/%s/DICOM", sessionData.getProject(), sessionData.getLabel(), scanData.getID()));
                    Path dicomDirPath = catalogFilePath.getParent();

                    Optional<File> file = Files.list( dicomDirPath)
                            .filter( Files::isRegularFile)
                            .filter(path -> path.toString().endsWith(".dcm"))
                            .map(Path::toFile)
                            .findFirst();

                    if( file.isPresent()) {
                        DICOMObject dicomObject = new DICOMObject( file.get());
                        reportRecord(dicomObject, subjectData, sessionData, scanData, ps);
                    }
                }
            }
        }
    }

    private static String recordFormat = "%s,%s,%s,%s,%s,%s,%s";

    public void reportHeader( PrintStream ps) {
        ps.println( String.format(recordFormat, "Project", "Subject", "Session", "Scan", "Attribute", "XNAT DB", "DICOM"));
    }

    public void reportRecord( DICOMObject dicomObject, SubjectData subjectData, ImageSessionData sessionData, ImageScanData scanData, PrintStream ps) {
        ps.println( String.format( recordFormat, subjectData.getProject(), subjectData.getLabel(), sessionData.getLabel(), scanData.getID(), "","",""));
        ps.println( String.format( recordFormat, "","","","", "PatientName", sessionData.getDcmPatientName(), dicomObject.getPatientName()));
        ps.println( String.format( recordFormat, "","","","", "PatientID", sessionData.getDcmPatientId(), dicomObject.getPatientID()));
        ps.println( String.format( recordFormat, "","","","", "Accession Number", sessionData.getDcmAccessionNumber(), dicomObject.getAccessionNumber()));
        ps.println( String.format( recordFormat, "","","","", "Modality", sessionData.getModality(), dicomObject.getModality()));
        ps.println( String.format( recordFormat, "","","","", "StudyID", sessionData.getStudyId(), dicomObject.getStudyID()));
    }

    public void reportStudiesInMultipleProjects( String projectLabel, String subjectLabel, Path archiveRootPath) throws IOException {
        Map<String, Integer> studyUIDs = xnatClient.getProjectCountPerStudyInstanceUID();
        for( String studyUID: studyUIDs.keySet()) {
            List<String> projectLabels = xnatClient.getProjectLabelsByStudyInstanceUID( studyUID);
        }

    }

}
