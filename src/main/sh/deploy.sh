#!/bin/bash

scp ../../../target/dwbootstrap-1.0-SNAPSHOT.jar dwreport.sh david.maffitt@mirrir-fs01.nrg.wustl.edu:/tmp

ssh mirrir-fs chmod a+rx /tmp/dwreport.sh /tmp/dwbootstrap-1.0-SNAPSHOT.jar

ssh -t mirrir-fs sudo bash -c 'cp /tmp/dwreport.sh /tmp/dwbootstrap-1.0-SNAPSHOT.jar /usr/local/bin; chmod a+rx /usr/local/bin/dw*'
