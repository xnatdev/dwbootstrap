package org.nrg.dicomweb.bootstrap;

import org.nrg.dicomweb.bootstrap.reporter.Reporter;
import org.nrg.dicomweb.bootstrap.reporter.ReporterDupStudyUIDs;
import org.nrg.xnatClient.XNATClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@ComponentScan
@PropertySource("application.properties")
public class ApplicationTestConfig {

    // env will have the properties defined in property source files.
    @Autowired
    private Environment env;

    // xnatDataSource is autoconfjigured by spring boot from params in application.properties
    @Autowired
    public DataSource xnatDataSource;

    @Bean(name="xnatClient")
    public XNATClient createXNATClient() throws IOException {
        return new XNATClient( xnatDataSource, env.getProperty( "rest.url"), env.getProperty( "rest.user"), env.getProperty( "rest.password"));
    }

    @Bean
    public Path getArchiveRootPath() {
        return Paths.get( env.getProperty("bootstrap.archiveRootPath"));
    }

    @Bean
    public Reporter createReporter( XNATClient xnatClient) {
        return new ReporterDupStudyUIDs( xnatClient);
    }

//    public DataSource createDataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName( env.getProperty( "jdbc.driver"));
//        dataSource.setUrl( env.getProperty( "jdbc.url"));
//        dataSource.setUsername( env.getProperty( "jdbc.user"));
//        dataSource.setPassword( env.getProperty( "jdbc.password"));
//
//        return dataSource;
//    }

}